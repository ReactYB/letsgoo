/* This example requires Tailwind CSS v2.0+ */
import { Fragment, useState } from "react";
import { Popover, Transition } from "@headlessui/react";
import {
  AtSymbolIcon,
  Bars3BottomRightIcon,
  ChevronDownIcon,
  PlayIcon,
  ShieldCheckIcon,
  XMarkIcon,
} from "@heroicons/react/20/solid";
import { NavLink } from "react-router-dom";
import YBToggleDark from "../YBToggleDark/YBToggleDark";
import YBLogo from "../YBLogo/YBLogo";

const solutions = [
  {
    name: "Soka Collection 2015",
    description: "Speak directly to your customers in a more meaningful way.",
    href: "#",
    icon: AtSymbolIcon,
  },
  {
    name: "Soka Collection 2016",
    description: "Your customers' data will be safe and secure.",
    href: "#",
    icon: ShieldCheckIcon,
  },
  {
    name: "Resort 2016 Collection",
    description: "Connect with third-party tools that you're already using.",
    href: "#",
    icon: AtSymbolIcon,
  },
];

const callsToAction = [{ name: "SEE ALL COLLECTIONS", href: "#", icon: PlayIcon }];

// Safer Mapping null undefined removed
function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(" ");
}

const Header = () => {
  const [isActive,setActive] = useState(true);
  return (
    <Popover className="h-[70px] sticky top-0 z-40 w-full px-2 py-1 transition-colors duration-200 ease-in-out rounded-b-lg bg-gradient-to-b from-gray-200/80 dark:from-gray-900/60 backdrop-blur sm:px-4">
      {({ open }) => (
        <>
          {/* top layer for mobile */}
          <div
            className="absolute inset-0 z-30 pointer-events-none"
            aria-hidden="true"
          />
          {/* top layer for desoktop */}
          <div className="relative z-20">
            <div className="max-w-7xl mx-auto flex px-1 py-3 items-center justify-between">
              {/* ICON container DESOKTOP */}
              <NavLink to={"/"} className="flex gap-2">
                <span className="sr-only">ChamssDoha</span>
                <div className="w-10">
                  <YBLogo />
                </div>
                <span className="block md:hidden  self-center text-xl font-semibold lg:block whitespace-nowrap dark:text-white">
                  logo
                </span>
              </NavLink>

              {/* OPEN MENU container MOBILE*/}
              <div className="-mr-2 -my-2 md:hidden">
                <Popover.Button className="rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 dark:hover:text-gray-400 dark:hover:bg-gray-700/80 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-slate-400">
                  <span className="sr-only">Open menu</span>
                  <Bars3BottomRightIcon
                    className="h-6 w-6"
                    aria-hidden="true"
                  />
                </Popover.Button>
              </div>

              {/*  MENU ITEMS DESKTOP */}
              <div className="hidden md:flex-1 relative md:flex md:items-center md:justify-between ">
                <Popover.Group
                  as="nav"
                  className="rtl:sm:mr-16 ltr:sm:ml-16 rtl:lg:mr-32 ltr:lg:ml-32 flex gap-12"
                >
                  {/*  COLLECTION PopOver ITEMS DESKTOP */}
                  <Popover>
                    {({ open }) => (
                      <>
                        <Popover.Button className="text-gray-900 relative group font-semibold inline-flex items-center  hover:bg-gray-100 md:hover:bg-transparent md:hover:text-black md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700">
                          {/*  COLLECTION BUTTON*/}
                          <span className="self-center">header.linkOne</span>
                          <ChevronDownIcon
                            className={classNames(
                              open
                                ? "text-gray-900 rotate-180"
                                : "text-gray-400",
                              "rtl:mr-2 ltr:ml-2 mt-1 h-5 w-5 group-hover:text-gray-500 transition-transform delay-150 duration-150"
                            )}
                            aria-hidden="true"
                          />
                        </Popover.Button>

                        <Transition
                          show={open}
                          as={Fragment}
                          enter="transition ease-out duration-200"
                          enterFrom="opacity-0 -translate-y-1"
                          enterTo="opacity-100 translate-y-0"
                          leave="transition ease-in duration-150"
                          leaveFrom="opacity-100 translate-y-0"
                          leaveTo="opacity-0 -translate-y-1"
                        >
                          {/*  COLLECTION ITEMS*/}
                          <Popover.Panel
                            static
                            className="hidden max-w-sm mt-4 rounded-xl md:block absolute z-10 inset-x-0 transform shadow-lg bg-slate-800"
                          >
                            {/*  COLLECTION GRID */}
                            <div className="mx-auto">
                              {solutions.map((item) => (
                                /*  COLLECTION LINK */
                                <a
                                  key={item.name}
                                  href={item.href}
                                  className="m-3 p-3 flex flex-col lg:justify-between rounded-lg"
                                >
                                  <div className="flex">
                                    {/*  COLLECTION ITEM IMAGE */}

                                    <span className="p-2 flex items-center justify-center rounded-md bg-indigo-500 text-white">
                                      <item.icon
                                        className="h-6 w-6"
                                        aria-hidden="true"
                                      />
                                    </span>

                                    {/*  COLLECTION ITEM DESCRIPTION */}
                                    <div className="flex items-center ml-3">
                                      <div>
                                        <p className="text-base md:text-xl uppercase font-medium text-gray-900 dark:text-white">
                                          {item.name}
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                </a>
                              ))}
                            </div>
                            {/*  COLLECTION SEE MORE */}
                            <div className="bg-gray-50">
                              <div className="max-w-7xl mx-auto text-center flex space-y-6 px-4 py-5 sm:flex sm:space-y-0 sm:space-x-10 sm:px-6   lg:px-8 md:items-center md:justify-center">
                                {callsToAction.map((item) => (
                                  <div key={item.name} className="flow-root ">
                                    <a
                                      href={item.href}
                                      className="-m-3 p-3 flex items-center rounded-md text-base  font-medium text-gray-900 hover:bg-gray-100"
                                    >
                                      <item.icon
                                        className="flex-shrink-0 h-6 w-6 text-gray-400"
                                        aria-hidden="true"
                                      />
                                      <span className="ml-3">{item.name}</span>
                                    </a>
                                  </div>
                                ))}
                              </div>
                            </div>
                          </Popover.Panel>
                        </Transition>
                      </>
                    )}
                  </Popover>
                  <NavLink
                    to={"/activities"}
                    type="button"
                    className={(isActive) =>
                      "block text-gray-700 font-semibold hover:bg-gray-100 md:hover:bg-transparent md:hover:text-black md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700" +
                      (isActive ? " text-primary/95 dark:text-primaryDark" : "")
                    }
                    aria-expanded="false"
                  >
                    <span className="self-center truncate">
                      header.linkTwo
                    </span>
                  </NavLink>
                  <NavLink
                    to={"/createActivity"}
                    type="button"
                    className={(isActive) =>
                      "block py-2 pr-4 pl-3 font-semibold text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-black md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700" +
                      (isActive ? " text-primary/95 dark:text-primaryDark" : "")
                    }
                    aria-expanded="false"
                  >
                    <span className="self-center truncate">
                      header.linkThree
                    </span>
                  </NavLink>
                </Popover.Group>

                <div className="flex items-center rtl:md:mr-12 ltr:md:ml-12">
                  {true ? (
                    <>
                      {" "}
                      <NavLink
                        to={"/register"}
                        className={(isActive) =>
                          "block truncate text-gray-700 font-semibold hover:bg-gray-100 md:hover:bg-transparent md:hover:text-black md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700" +
                          (isActive
                            ? " text-primary/95 dark:text-primaryDark"
                            : "")
                        }
                      >
                        login.signUp
                      </NavLink>
                      <button
                        onClick={()=>{}}
                        className=" self-center truncate rtl:mr-8 ltr:ml-8 inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-primary hover:bg-primary/75 dark:bg-primaryDark dark:hover:bg-primaryDark/75"
                      >
                        login.signIn
                      </button>
                    </>
                  ) : (
                    <NavLink
                      to={"/Profil"}
                      className="self-center truncate rtl:mr-8 ltr:ml-8 inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-primary hover:bg-primary/75 dark:bg-primaryDark dark:hover:bg-primaryDark/75"
                    >
                     login.Profile
                    </NavLink>
                  )}
                  <YBToggleDark className="rtl:mr-3 ltr:ml-3" />
                  {/* <YBToggleLang className="rtl:mr-3 ltr:ml-3" /> */}
                </div>
              </div>
            </div>
          </div>
          {/* Pop over Mobile */}
          <Transition
            show={open}
            as={Fragment}
            enter="duration-200 ease-out"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="duration-100 ease-in"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <Popover.Panel
              focus
              static
              className="absolute z-30 top-0 inset-x-0 transition transform origin-top-right md:hidden"
            >
              <div className="px-2 py-1 shadow-lg ring-1 ring-black ring-opacity-5 bg-gray-50 border-t border-x border-gray-200 rounded-b-lg dark:border-gray-700 dark:bg-gray-800 ">
                <div className="py-3 px-1">
                  <div className="flex items-center justify-between">
                    <div className="w-10">
                      <YBLogo />
                    </div>
                    <div className="-mr-2">
                      <Popover.Button className="rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 dark:hover:text-gray-400 dark:hover:bg-gray-700/80 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-slate-400">
                        <span className="sr-only">Close menu</span>
                        <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                      </Popover.Button>
                    </div>
                  </div>
                  <div className="flex items-center justify-center">
                    <YBToggleDark className="rtl:mr-3 ltr:ml-3" />
                    {/* <YBToggleLang className="rtl:mr-3 ltr:ml-3" /> */}
                  </div>
                </div>
              </div>
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  );
};
export default Header;

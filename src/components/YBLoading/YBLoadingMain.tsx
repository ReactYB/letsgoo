const YBLoadingMain = () => {
  return (
      <section className="max-w-7 xlrelative flex content-center justify-center h-screen gap-4 mt-72">
        <div className="absolute bg-primary/10 dark:bg-primaryDark/10 w-32 h-32rounded-full shadow-xl animate-ping delay-5s"></div>
        <div className="bg-primary/60 dark:bg-primaryDark/60 w-24 h-24 mt-[16px] absolute animate-ping rounded-full shadow-xl"></div>
        <div className=" bg-primary dark:bg-primaryDark  w-16 h-16 absolute mt-[32px] animate-pulse rounded-full shadow-xl"></div>
      </section>
  );
};

export default YBLoadingMain;

const YBArrowRight = (props: any) => {
  const { onClick } = props;
  return (
    <>
      {onClick && (
        <div onClick={onClick}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            className="z-10 absolute -right-4  top-1/2 w-8 h-8 sm:w-8 sm:h-8 -translate-y-1/2 rounded-full cursor-pointer block bottom-10 bg-transparent text-primary dark:text-primaryDark"
          >
            <path
              fill="currentColor"
              fillRule="evenodd"
              d="M10.707 18.364a1 1 0 0 1-1.414-1.414l4.95-4.95-4.95-4.95a1 1 0 0 1 1.414-1.414l5.657 5.657a1 1 0 0 1 .078 1.327l-.078.087-5.657 5.657z"
            ></path>
          </svg>
        </div>
      )}
    </>
  );
};

export default YBArrowRight;

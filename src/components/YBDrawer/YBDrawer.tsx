import { observer } from "mobx-react-lite";
import { useTranslation } from "react-i18next";
import { NavLink } from "react-router-dom";
import { useStore } from "../../stores/store";
import YBUserIcon from "../YBUserIcon/YBUserIcon";

const YBDrawer = () => {
  const { navStore, userStore } = useStore();
  const { t } = useTranslation();
  return (
    <>
      {/* button float */}
      <button
        type="button"
        onClick={navStore.setMiniSidenav}
        className="fixed bottom-0 z-40 p-2 m-4 rounded-full bg-gray-100/75 dark:bg-slate-900/60 backdrop-blur dark:bg-gray-900 shadow-xl"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-6 h-6 text-slate-900 dark:text-gray-100"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M9.594 3.94c.09-.542.56-.94 1.11-.94h2.593c.55 0 1.02.398 1.11.94l.213 1.281c.063.374.313.686.645.87.074.04.147.083.22.127.324.196.72.257 1.075.124l1.217-.456a1.125 1.125 0 011.37.49l1.296 2.247a1.125 1.125 0 01-.26 1.431l-1.003.827c-.293.24-.438.613-.431.992a6.759 6.759 0 010 .255c-.007.378.138.75.43.99l1.005.828c.424.35.534.954.26 1.43l-1.298 2.247a1.125 1.125 0 01-1.369.491l-1.217-.456c-.355-.133-.75-.072-1.076.124a6.57 6.57 0 01-.22.128c-.331.183-.581.495-.644.869l-.213 1.28c-.09.543-.56.941-1.11.941h-2.594c-.55 0-1.02-.398-1.11-.94l-.213-1.281c-.062-.374-.312-.686-.644-.87a6.52 6.52 0 01-.22-.127c-.325-.196-.72-.257-1.076-.124l-1.217.456a1.125 1.125 0 01-1.369-.49l-1.297-2.247a1.125 1.125 0 01.26-1.431l1.004-.827c.292-.24.437-.613.43-.992a6.932 6.932 0 010-.255c.007-.378-.138-.75-.43-.99l-1.004-.828a1.125 1.125 0 01-.26-1.43l1.297-2.247a1.125 1.125 0 011.37-.491l1.216.456c.356.133.751.072 1.076-.124.072-.044.146-.087.22-.128.332-.183.582-.495.644-.869l.214-1.281z"
          />
          <path strokeLinecap="round" strokeLinejoin="round" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z" />
        </svg>
      </button>
      {/* SideNav Style */}
      <div
        className={`${
          navStore.miniSidenav ? "md:translate-y-16 " : "translate-y-[calc(100%_-_3rem)] "
        } w-full md:w-4/12 lg:w-5/12 xl:w-2/12 transition-transform ease-in-out duration-500 rtl:left-0 ltr:right-0 fixed z-50 h-full overflow-y-auto bg-gray-50 border-t border-x border-gray-200 rounded-t-lg dark:border-gray-700 dark:bg-gray-800 `}
      >
        {/* Header of sideNav */}
        <div
          className="p-2 cursor-pointer hover:bg-gray-100 dark:hover:bg-gray-700/75"
          onClick={() => navStore.setMiniSidenav()}
        >
          <span className="absolute w-8 h-1 -translate-x-1/2 bg-gray-300 rounded-lg top-3 left-1/2 dark:bg-gray-600"></span>
          <h5 className="inline-flex gap-2 items-center text-base text-gray-500 dark:text-gray-400">
            <YBUserIcon size="small" />
            {t("login.account")}
          </h5>
        </div>
        <div className="w-full bg-white dark:bg-gray-800">
          <div className="flex flex-col items-center py-4">
            <YBUserIcon size="large" />
            <h5 className="mt-4 mb-1 text-xl font-medium text-gray-900 dark:text-white">
              {userStore.user?.displayName}
            </h5>
            <span className="text-sm text-gray-500 dark:text-gray-400">
              {userStore.user?.email}
            </span>
            <div className="flex gap-2 mt-4 space-x-3 md:mt-6">
              {userStore.isLoggedIn ? (
                <button
                  onClick={userStore.logout}
                  className="flex items-center h-12 px-6 text-sm font-semibold text-white rounded-lg pointer-events-auto bg-slate-900 hover:bg-slate-700 focus:outline-none dark:bg-slate-700 dark:hover:bg-slate-600"
                >
                  {t("login.Logout")}
                </button>
              ) : (
                <>
                  <NavLink
                    to={"/Login"}
                    onClick={navStore.setMiniSidenav}
                    className=" self-center truncate  inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-primary hover:bg-primary/75 dark:bg-primaryDark dark:hover:bg-primaryDark/75"
                  >
                    {t("login.signIn")}
                  </NavLink>
                </>
              )}
            </div>
          </div>
        </div>

        {/* <div className="grid grid-cols-3 gap-2 p-2 lg:grid-cols-3">
          <div className="p-4 rounded-lg cursor-pointer bg-gray-50 hover:bg-gray-100 dark:hover:bg-gray-600 dark:bg-gray-700">
            <div
              className={`flex justify-center items-center p-2 mx-auto mb-2  bg-gray-200 dark:bg-gray-500 rounded-full`}
            >
              <svg
                className="inline  text-gray-500 dark:text-gray-400"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"></path>
                <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"></path>
              </svg>
            </div>
            <div className="font-medium text-center text-gray-500 dark:text-gray-400">Chart</div>
          </div>
          <div className="p-4 rounded-lg cursor-pointer bg-gray-50 hover:bg-gray-100 dark:hover:bg-gray-600 dark:bg-gray-700">
            <div
              className={`flex justify-center items-center p-2 mx-auto mb-2  bg-gray-200 dark:bg-gray-500 rounded-full`}
            >
              <svg
                className="inline  text-gray-500 dark:text-gray-400"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"></path>
                <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"></path>
              </svg>
            </div>
            <div className="font-medium text-center text-gray-500 dark:text-gray-400">Chart</div>
          </div>
          <div className="p-4 rounded-lg cursor-pointer bg-gray-50 hover:bg-gray-100 dark:hover:bg-gray-600 dark:bg-gray-700">
            <div
              className={`flex justify-center items-center p-2 mx-auto mb-2  bg-gray-200 dark:bg-gray-500 rounded-full`}
            >
              <svg
                className="inline  text-gray-500 dark:text-gray-400"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"></path>
                <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"></path>
              </svg>
            </div>
            <div className="font-medium text-center text-gray-500 dark:text-gray-400">Chart</div>
          </div>
          <div className="p-4 rounded-lg cursor-pointer bg-gray-50 hover:bg-gray-100 dark:hover:bg-gray-600 dark:bg-gray-700">
            <div
              className={`flex justify-center items-center p-2 mx-auto mb-2  bg-gray-200 dark:bg-gray-500 rounded-full`}
            >
              <svg
                className="inline  text-gray-500 dark:text-gray-400"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"></path>
                <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"></path>
              </svg>
            </div>
            <div className="font-medium text-center text-gray-500 dark:text-gray-400">Chart</div>
          </div>
          <div className="p-4 rounded-lg cursor-pointer bg-gray-50 hover:bg-gray-100 dark:hover:bg-gray-600 dark:bg-gray-700">
            <div
              className={`flex justify-center items-center p-2 mx-auto mb-2  bg-gray-200 dark:bg-gray-500 rounded-full`}
            >
              <svg
                className="inline  text-gray-500 dark:text-gray-400"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"></path>
                <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"></path>
              </svg>
            </div>
            <div className="font-medium text-center text-gray-500 dark:text-gray-400">Chart</div>
          </div>
        </div> */}
        {/* Player  */}
        {/* <div className="relative inset-0 bottom-0 ">
          <div className="p-4 pb-6 space-y-6 bg-white border-b border-slate-100 dark:bg-slate-800 dark:border-slate-500 rounded-t-xl sm:p-10 sm:pb-8 lg:p-6 xl:p-10 xl:pb-8 sm:space-y-8 lg:space-y-6 xl:space-y-8">
            <div className="flex items-center space-x-4">
              <img
                src="/full-stack-radio.png"
                alt=""
                width="88"
                height="88"
                className="flex-none w-16 h-16 rounded-lg bg-slate-100"
                loading="lazy"
              />
              <div className="flex-auto min-w-0 space-y-1 font-semibold">
                <p className="text-sm leading-6 text-blue-700 dark:text-blue-500">
                  <abbr title="Episode">Ep.</abbr> 128
                </p>
                <h2 className="text-sm leading-6 truncate text-slate-500 dark:text-slate-400">
                  Scaling CSS at Heroku with Utility Classes
                </h2>
                <p className="text-lg text-slate-900 dark:text-slate-50">Full Stack Radio</p>
              </div>
            </div>
            <div className="space-y-2">
              <div className="relative">
                <div className="overflow-hidden rounded-full bg-slate-100 dark:bg-slate-700">
                  <div className="w-1/2 h-2 bg-blue-700 dark:bg-blue-700">a</div>
                </div>
                <div className="absolute flex items-center justify-center w-4 h-4 -mt-2 -ml-2 bg-white rounded-full shadow ring-blue-700 dark:ring-blue-700 ring-2 left-1/2 top-1/2">
                  <div className="w-1.5 h-1.5 bg-blue-700 dark:bg-blue-700 rounded-full ring-1 ring-inset ring-slate-900/5"></div>
                </div>
              </div>
              <div className="flex justify-between text-sm font-medium leading-6 tabular-nums">
                <div className="text-blue-700 dark:text-slate-100">24:16</div>
                <div className="text-slate-500 dark:text-slate-400">75:50</div>
              </div>
            </div>
          </div>
          <div className="flex items-center bg-slate-50 text-slate-500 dark:bg-slate-600 dark:text-slate-200 rounded-b-xl">
            <div className="flex items-center flex-auto justify-evenly">
              <button type="button" aria-label="Add to favorites">
                <svg width="24" height="24">
                  <path
                    d="M7 6.931C7 5.865 7.853 5 8.905 5h6.19C16.147 5 17 5.865 17 6.931V19l-5-4-5 4V6.931Z"
                    fill="currentColor"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>
              </button>
              <button
                type="button"
                className="hidden sm:block lg:hidden xl:block"
                aria-label="Previous"
              >
                <svg width="24" height="24" fill="none">
                  <path
                    d="m10 12 8-6v12l-8-6Z"
                    fill="currentColor"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M6 6v12"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>
              </button>
              <button type="button" aria-label="Rewind 10 seconds">
                <svg width="24" height="24" fill="none">
                  <path
                    d="M6.492 16.95c2.861 2.733 7.5 2.733 10.362 0 2.861-2.734 2.861-7.166 0-9.9-2.862-2.733-7.501-2.733-10.362 0A7.096 7.096 0 0 0 5.5 8.226"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M5 5v3.111c0 .491.398.889.889.889H9"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>
              </button>
            </div>
            <button
              type="button"
              className="flex items-center justify-center flex-none w-16 h-16 mx-auto -my-2 bg-white rounded-full shadow-md text-slate-900 dark:bg-slate-100 dark:text-slate-700 ring-1 ring-slate-900/5"
              aria-label="Pause"
            >
              <svg width="30" height="32" fill="currentColor">
                <rect x="6" y="4" width="4" height="24" rx="2" />
                <rect x="20" y="4" width="4" height="24" rx="2" />
              </svg>
            </button>
            <div className="flex items-center flex-auto justify-evenly">
              <button type="button" aria-label="Skip 10 seconds">
                <svg width="24" height="24" fill="none">
                  <path
                    d="M17.509 16.95c-2.862 2.733-7.501 2.733-10.363 0-2.861-2.734-2.861-7.166 0-9.9 2.862-2.733 7.501-2.733 10.363 0 .38.365.711.759.991 1.176"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M19 5v3.111c0 .491-.398.889-.889.889H15"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>
              </button>
              <button
                type="button"
                className="hidden sm:block lg:hidden xl:block"
                aria-label="Next"
              >
                <svg width="24" height="24" fill="none">
                  <path
                    d="M14 12 6 6v12l8-6Z"
                    fill="currentColor"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M18 6v12"
                    stroke="currentColor"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>
              </button>
              <button
                type="button"
                className="px-2 text-xs font-semibold leading-6 rounded-lg ring-2 ring-inset ring-slate-500 text-slate-500 dark:text-slate-100 dark:ring-0 dark:bg-slate-500"
              >
                1x
              </button>
            </div>
          </div>
        </div> */}
      </div>
      <section
        className={`${
          navStore.miniSidenav
            ? "transition-opacity translate-x-0 cursor-pointer"
            : "transition-all opacity-0 -z-10 hidden "
        } fixed overflow-hidden z-10 bg-gray-900/25 inset-0 transform ease-in-out`}
        onClick={() => {
          navStore.setMiniSidenav();
        }}
      ></section>
    </>
  );
};
export default observer(YBDrawer);

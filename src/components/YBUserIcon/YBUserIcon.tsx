import { observer } from "mobx-react-lite";
import { useStore } from "../../stores/store";

interface Props {
  size?: string;
  variant?: string;
  className?:string;
  status?:boolean
}  
const YBUserIcon = ({ variant="medium" , size="medium",className="none", status=true}: Props) => {
const buttonSizeClasses: any = {
    small: `h-8 w-8`,
    medium: `h-16 w-16`,
    large: `h-24 w-24`,
    Xlarge: `h-48 w-48`,
  };
  const dotSizeClasses: any = {
    small: `h-4 w-4`,
    medium: `h-6 w-6`,
    large: `h-10 w-10`,
    Xlarge: `h-20 w-20`,
  };

  const { userStore } = useStore();
  return (
    <div className={`relative ${className} rounded-full`}>
      {userStore.user ? (
        <img
          className={`${buttonSizeClasses[size]} rounded-full shadow-lg `}
          src="https://picsum.photos/200"
          alt={userStore.user.displayName}
        />
      ) : (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className={`${buttonSizeClasses[size]}  rtl:ml-2 ltr:mr-2 text-base text-gray-500 dark:text-gray-400`}
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
          />
        </svg>
      )}
      {status && <span
        className={`${userStore.user ? "bg-green-400" : "bg-gray-500"} ${
          dotSizeClasses[size]
        }  -bottom-[10%] right-[10%] absolute border-2 border-white dark:border-gray-800 rounded-full`}
      ></span>}
      
    </div>
  );
};

export default observer(YBUserIcon);

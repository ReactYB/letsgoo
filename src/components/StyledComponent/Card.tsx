import React from "react";
import { addTask, delTask } from "../../context/actions";
import { useAppState } from "../../context/AppStateContext";
import { CardContainer } from "./styles";

type CardProps = {
  text: string;
  id: string;
  tasksId:string;
};
export const Card = ({ text, id, tasksId }: CardProps) => {
  const { dispatch } = useAppState();
  return (
    <>
      <CardContainer>
        {text}
        <button
          className="h-full bg-slate-200 hover:bg-slate-300 top-0 right-0 absolute px-1"
          onClick={() => dispatch(delTask(id, tasksId))}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
            fill="currentColor"
            className="w-5 h-5"
          >
            <path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z" />
          </svg>
        </button>
      </CardContainer>
    </>
  );
};

export default Card;

import styled from 'styled-components'
type AddItemButtonProps = {
  dark?: boolean
}
// https://library.samdu.uz/files/5ed90af75fa83c2c8954ef8368b156ba_Ivanov_M_,_Bespoyasov_A_Fullstack_React_with_TypeScript_Revision.pdf
export const AppContainer = styled.div` 
display : flex;
flex-wrap: wrap;
 
min-height: 50vh;
`
export const ColumnContainer = styled.div` 
  flex: 1 0 calc(25% - 20px);
  margin: 10px;
  background-color: #F6F1F1;
  border-radius : 15px;
  border : 3px solid #E4D0D0;
  padding : 10px 20px 10px 20px;
  height:100%;
`
export const ColumnTitle = styled.h1` 
font-weight : Bold;
`
export const CardContainer = styled.div` 
display : flex;
position:relative;
overflow:hidden;
padding :5px 0px 5px 10px ;
justify-content:space-between;
margin : 10px 0px;
background-color : #fff;
border-radius : 4px;
cursor: pointer;
box-shadow: #091e4240 0px 1px 0px 0px;
`
export const AddItemButton = styled.button<AddItemButtonProps>` 
background-color: #ffffff3d;
border-radius: 4px;
border: none;
color: ${props => (props.dark ? "#000" : "#fff")};
cursor: pointer;
 height: 100%;
padding: 10px 12px;
text-align: left;
transition: background 85ms ease-in;
&:hover {
background-color: #fff;
}
`
export const NewItemFormContainer = styled.div`
 
`
export const NewItemButton = styled.button`
background-color: #5aac44;
border-radius: 3px;
border: none;
box-shadow: none;
color: #fff;
padding: 6px 12px;
text-align: center;
`
export const NewItemInput = styled.input`
border-radius: 3px;
border: none;
box-shadow: #091e4240 0px 1px 0px 0px;
padding: 0.5rem 1rem;
width:100%;
margin-bottom:0.5rem
`
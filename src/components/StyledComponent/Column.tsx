import { ColumnContainer, ColumnTitle } from "./styles";
import { FC } from "react";
import AddNewItem from "./AddNewItem";
import { useAppState } from "../../context/AppStateContext";
import Card from "./Card";
import { addTask } from "../../context/actions";

type ColumnProps = {
  text: string;
  id: string;
};

const Column: FC<ColumnProps> = ({ text,id }) => {
  const { getTasksByListId } = useAppState();
  const tasks = getTasksByListId(id);
  const { lists, dispatch } = useAppState();
  return (
    <ColumnContainer>
      <ColumnTitle>{text}</ColumnTitle>
      {tasks.map((task) => (
        <Card text={task.text} key={task.id} id={task.id} tasksId={id} />
      ))}
      <AddNewItem
        toggleButtonText="+ Add another list"
        onAdd={(text) => dispatch(addTask(text, id))}
        dark
      />
    </ColumnContainer>
  );
};

export default Column;

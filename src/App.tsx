import { useReducer } from "react";
import "./assets/css/tailwind.css";
import Footer from "./components/footer/Footer";
import Header from "./components/header/Header";
import AddNewItem from "./components/StyledComponent/AddNewItem";
import Column from "./components/StyledComponent/Column";
import {
  AddItemButton,
  AppContainer,
} from "./components/StyledComponent/styles";
import YBAccordion from "./components/YBAccordion/YBAccordion";
import YBConsole from "./components/YBConsole/YBConsole";
import YBLoadingMain from "./components/YBLoading/YBLoadingMain";
import { addList } from "./context/actions";
import { useAppState } from "./context/AppStateContext";
interface State {
  count: number;
}
interface Action {
  type: "increment" | "decrement";
}

function App() {
  const counterReducer = (state: State, action: Action) => {
    switch (action.type) {
      case "increment":
        return { count: state.count + 1 };
      case "decrement":
        return { count: state.count - 1 };
      default:
        throw new Error();
    }
  };

  const [state, dispatchA] = useReducer(counterReducer, { count: 0 });
  const { lists,dispatch } = useAppState();
  
  return (
    <>
      <Header />
      <AppContainer className="max-w-7xl mx-auto">
        {lists.map((list) => (
          <Column text={list.text} key={list.id} id={list.id} />
        ))}
        <AddNewItem
          toggleButtonText="+ Add another list"
          onAdd={(text) => dispatch(addList(text))}
          dark
        />
      </AppContainer>
      <AddItemButton
        dark={false}
        onClick={() => dispatchA({ type: "increment" })}
      >
        {state.count}
      </AddItemButton>
      <AddItemButton
        dark={false}
        onClick={() => dispatchA({ type: "decrement" })}
      >
        {state.count}
      </AddItemButton>

      <YBLoadingMain />
      <YBConsole />
      <YBAccordion />
      <Footer />
    </>
  );
}

export default App;

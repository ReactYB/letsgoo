// Interfaces declaration
// Types of == Interface seul probleme c'est que les types ne sont pas extendable
interface AddListAction {
    type: "ADD_LIST"
    payload: string
}
interface AddTaskAction {
    type: "ADD_TASK"
    payload: { text: string; listId: string }
}

interface DelTaskAction {
    type: "DEL_TASK"
    payload: { id: string, listId: string }
}

interface MoveListAction {
    type: "MOVE_LIST"
    payload: { draggedId: string; hoverId: string }
}


export type Action = AddListAction | AddTaskAction | MoveListAction | DelTaskAction

export const addTask = (
    text: string,
    listId: string,
): Action => ({
    type: "ADD_TASK",
    payload: {
        text,
        listId
    }
})
export const addList = (
    text: string,
): Action => ({
    type: "ADD_LIST",
    payload: text
})

export const delTask = (
    id: string, listId: string
): Action => ({
    type: "DEL_TASK",
    payload: {
        id, listId
    }
})

export const moveList = (
    draggedId: string,
    hoverId: string,
): Action => ({
    type: "MOVE_LIST",
    payload: {
        draggedId,
        hoverId,
    }
})
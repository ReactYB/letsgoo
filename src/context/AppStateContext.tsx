import { createContext, useContext, FC, Dispatch } from "react";
import { Action } from "./actions";
import { AppState, List, Task, reducer } from "./reducer";
import { useImmerReducer } from "use-immer";

interface AppStateContextProps {
  lists: List[];
  getTasksByListId(id: string): Task[];
  dispatch: Dispatch<Action>;
};
const appData: AppState = {
  lists: [
    {
      id: "0",
      text: "To Do",
      tasks: [{ id: "c0", text: "Generate app scaffold" }],
    },
    {
      id: "1",
      text: "In Progress",
      tasks: [
        { id: "c20", text: "Learn Typescript" },
        { id: "c21", text: "Learn Typescript" },
        { id: "c22", text: "Learn Typescript" },
      ],
    },
    {
      id: "2",
      text: "Done",
      tasks: [
        { id: "c30", text: "Begin to use static typing" },
        { id: "c31", text: "Begin to use static typing" },
        { id: "c32", text: "Begin to use static typing" },
        { id: "c33", text: "Begin to use static typing" },
      ],
    },
  ],
};
const AppStateContext = createContext<AppStateContextProps>(
  {} as AppStateContextProps
);

interface Props {
  children: React.ReactNode;
}

export const AppStateProvider: FC <Props> = ({ children }) => {
  const [state, dispatch] = useImmerReducer(reducer, appData);
  const { lists } = state;
  const getTasksByListId = (id: string) => {
    return lists.find((list) => list.id === id)?.tasks || [];
  };
  return (
    <AppStateContext.Provider value={{ lists, getTasksByListId, dispatch }}>
      {children}
    </AppStateContext.Provider>
  );
};
export const useAppState = () => {
  return useContext(AppStateContext);
};
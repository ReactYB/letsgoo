import { Action } from './actions'
import { nanoid } from "nanoid"
import { findItemIndexById, removeItemAtIndex } from '../Utils/utils';

export interface Task {
    id: string;
    text: string;
};
export interface List {
    id: string;
    text: string;
    tasks: Task[];
};
export interface AppState {
    lists: List[];
};

export const reducer = (draft: AppState, action: Action): AppState | void => {
    switch (action.type) {
        case "ADD_LIST": {
            draft.lists.push({
                id: nanoid(),
                text: action.payload,
                tasks: []
            })
            break
        };
        case "ADD_TASK": {
            const { text, listId } = action.payload
            const targetListIndex = findItemIndexById(draft.lists, listId)
            draft.lists[targetListIndex].tasks.push({
                id: nanoid(),
                text
            })
            break
        };
        case "DEL_TASK": {
            const { id, listId } = action.payload
            const targetListIndex = findItemIndexById(draft.lists, listId)
            draft.lists[targetListIndex].tasks = draft.lists[targetListIndex].tasks.filter(item => item.id != id)
            break
        };
        case "MOVE_LIST": {
            const { draggedId, hoverId } = action.payload
            
            break
        }
        default: {
            break
        }
    }
}